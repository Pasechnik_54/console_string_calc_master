﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ConsoleCalculator.src.Expression;

using MyListContainer;

namespace ConsoleCalculator.src
{
    using validSymbols = ValidSymbolsTable;

    class CalcExpr : IStrCalculator
    {
        private MyList<Item> _parsedExpression = new MyList<Item>();
        private ExpressionCreator _fillerForCalcTree = new ExpressionCreator();


        public bool TryCalcExpression(string expressionForCalc, ref string calcResult, ref string errorMsg)
        {

            if ( ExprGenericFormatNoValid(expressionForCalc, ref errorMsg) )
            {
                return false;
            }

            bool parseResultOk = ParseExpression(expressionForCalc, ref errorMsg);

            if (parseResultOk)
            {
                calcResult = _fillerForCalcTree.CalcExpression(ref errorMsg);
                if ( errorMsg == string.Empty)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
                        
        }


        private bool ParseExpression(string expr, ref string errMsg)
        {
            _parsedExpression.Clear();

            string numberBuf = "";

            int i = 0;
            while (i < expr.Length)
            {
                char currSymbol = expr[i];

                SymbolType currType = validSymbols.GetSymbolType(currSymbol);

                switch (currType)
                {
                    case SymbolType.UnknownSymbol: // ошибка парсинга - неизвестный символ
                        {
                            errMsg = string.Format("Error: unknown symbol {0}", currSymbol);                            
                        }
                        return false;
                    case SymbolType.PartOfNumber:
                        {
                            numberBuf += currSymbol;
                            i++;
                        }
                        break;
                    case SymbolType.Operator:
                        {
                            SaveNumberAndClearBuf(ref numberBuf);

                            SaveSpecialSymbol(ItemType.BinaryOperator, currSymbol);
                            i++;
                        }
                        break;
                    case SymbolType.OpenedBrace:
                        {
                            SaveNumberAndClearBuf(ref numberBuf);

                            SaveSpecialSymbol(ItemType.OpenBrace, currSymbol);
                            i++;
                        }
                        break;
                    case SymbolType.ClosedBrace:
                        {
                            SaveNumberAndClearBuf(ref numberBuf);

                            SaveSpecialSymbol(ItemType.CloseBrace, currSymbol);

                            i++;
                        }
                        break;
                    case SymbolType.IgnoredSymbol:
                        {
                            SaveNumberAndClearBuf(ref numberBuf);
                            i++;
                        }
                        break;
                }
            }
            SaveNumberAndClearBuf(ref numberBuf);

            // debug
            /*
            foreach (var s in _parsedExpression)
            {
                Console.WriteLine("{0}  ({1})", s.StringExpression, s.Type.ToString() );
            }
            */

            bool resFillTreeOk = false;
            resFillTreeOk = _fillerForCalcTree.CreateCalcExpression(_parsedExpression, 
                                                                        ref errMsg);

            if (!resFillTreeOk)
            {                
                return false;
            }


            return true;
        }

        private void SaveNumberAndClearBuf(ref string buf)
        {
            if (buf != string.Empty)
            {
                Item numbItem = new Item(ItemType.Number, buf);
                _parsedExpression.Add(numbItem);
                buf = "";
            }
        }

        private void SaveSpecialSymbol(ItemType type, char symbol)
        {
            string symbStr = new string(symbol, 1);
            Item specialItem = new Item(type, symbStr);
            _parsedExpression.Add(specialItem);
        }

       
        private bool ExprGenericFormatNoValid(string expr, ref string errMsg)
        {
            const int MIN_LENGTH_FOR_CALCULATED_EXPRESSION = 3;

            // возможно сделать не константой, а задавать в конструкторе
            const int MAX_LENGTH_FOR_CALCULATED_EXPRESSION = 80;
            const string errHeader = "Error: ";

            
            if (expr.Length < MIN_LENGTH_FOR_CALCULATED_EXPRESSION)
            {
                errMsg = errHeader + "expression is too short";
                return true;
            }
            else if (expr.Length > MAX_LENGTH_FOR_CALCULATED_EXPRESSION)
            {
                errMsg = errHeader + "expression is too long";
                return true;
            }
            else if ( ExprContainsUnacceptSymbols(expr) )
            {
                errMsg = errHeader + "expression contains unacceptable symbols";
                return true;
            }
            else if ( IncorrectParenthesesOrder(expr) )
            {
                errMsg = errHeader + "incorrect parentheses order in expression";
                return true;
            }
            // это чтобы не выйти за пределы индекса при парсинге операндов            
            else if ( validSymbols.SymbolIsUnacceptableForStartExpression( expr.First() ) )
            {
                errMsg = errHeader + "unacceptable first symbol";
                return true;
            }
            else if (validSymbols.SymbolIsUnacceptaForEndExpression(expr.Last() ) )
            {
                errMsg = errHeader + "unacceptable last symbol";
                return true;
            }                                 

            return false;
        }


        public bool ExprContainsUnacceptSymbols(string expr)
        {           

            foreach (var currSymbol in expr)
            {
                SymbolType type = validSymbols.GetSymbolType(currSymbol);
                
                if ( type == SymbolType.PartOfNumber ||
                     type == SymbolType.OpenedBrace ||
                     type == SymbolType.ClosedBrace ||
                     type == SymbolType.Operator ||
                     type == SymbolType.IgnoredSymbol)
                {
                    continue;
                }
                else
                {
                    return true;
                }
            }

            return false;
        }



        // правильное расположение скобок        
        public bool IncorrectParenthesesOrder(string expr)
        {
            int requiredClosed = 0;

            foreach (var currChar in expr)
            {
                if (currChar == validSymbols.OpenRoundBrace)
                {
                    requiredClosed++;
                }

                if (currChar == validSymbols.ClosedRoundBrace)
                {
                    // проверить, что левее есть свободная открывающая скобка
                    if (requiredClosed > 0)
                    {
                        requiredClosed--;
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            if (requiredClosed == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
