﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator.src
{
    class Program
    {
        static void Main(string[] args)
        {
            IStrCalculator calc = new CalcExpr();
            ICalcUI uiControl = new CalculatorConsoleUI(calc);

            uiControl.FirstExpressionInput();
        }
    }
}
