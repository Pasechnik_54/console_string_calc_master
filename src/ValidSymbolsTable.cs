﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ConsoleCalculator.src.Expression;
using MyListContainer;

namespace ConsoleCalculator.src
{
    static class ValidSymbolsTable
    {
        public static char OpenRoundBrace { get { return BRACES_CHARACTERS_LIST[0]; } }
        public static char ClosedRoundBrace { get { return BRACES_CHARACTERS_LIST[1]; } }
        public static char OperatorDiff 
        { 
            get 
            { 
                foreach (var pair in OPERATOR_CHARACTER_TABLE)
                {
                    if (pair.Key == OperationType.Diff)
                    {
                        return pair.Value;
                    }
                }

                // вернем пробел, если не нашли в списке
                return SPACE;
            } 
        }

        // список спецсимволов
        private const char OPENED_BRACE = '(';
        private const char CLOSED_BRACE = ')';

        private const char MUL = '*';
        private const char DIV = '/';
        private const char PLUS = '+';
        private const char MINUS = '-';

        private const char FLOAT_SEPARATOR = ',';

        private const char SPACE = ' '; // чтобы игнорировать пробелы

        private static readonly char[] NUMBER_CHARACTERS_LIST =
        {
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            FLOAT_SEPARATOR
        };
                            

        private static readonly char[] BRACES_CHARACTERS_LIST =
        {
            OPENED_BRACE,
            CLOSED_BRACE
        };

        private static readonly MyList<KeyValuePair<OperationType, char>> OPERATOR_CHARACTER_TABLE = 
        new MyList<KeyValuePair<OperationType, char>>
        {
            { new KeyValuePair<OperationType, char>(OperationType.Mul, MUL) },
            { new KeyValuePair<OperationType, char>(OperationType.Div, DIV) },
            { new KeyValuePair<OperationType, char>(OperationType.Sum, PLUS) },
            { new KeyValuePair<OperationType, char>(OperationType.Diff, MINUS) }
            
        };
        
        private static readonly char[] UNACCEPTABLE_FROM_FIRST_IN_EXPR =
        {
            MUL,
            DIV,
            PLUS,
            CLOSED_BRACE
        };

        private static readonly char[] UNACCEPTABLE_FROM_END_IN_EXPR =
        {
            MUL,
            DIV,
            PLUS,
            MINUS,
            OPENED_BRACE
        };
        
        

        public static bool SymbolIsUnacceptableForStartExpression(char symbol)
        {
            return  ResultFindInTargetList(symbol, UNACCEPTABLE_FROM_FIRST_IN_EXPR);
        }

        public static bool SymbolIsUnacceptaForEndExpression(char symbol)
        {
           return ResultFindInTargetList(symbol, UNACCEPTABLE_FROM_END_IN_EXPR);
        }


        public static SymbolType GetSymbolType(char symbol)
        {
            if ( SymbolIsPartOfNumber(symbol) )
            {
                return SymbolType.PartOfNumber;
            }
            else if ( SymbolIsArithmeticOperator(symbol) )
            {
                return SymbolType.Operator;
            }
            else if ( symbol == OPENED_BRACE )
            {
                return SymbolType.OpenedBrace;
            }
            else if (symbol == CLOSED_BRACE)
            {
                return SymbolType.ClosedBrace;
            }
            else if (symbol == SPACE)
            {
                return SymbolType.IgnoredSymbol;
            }

            return SymbolType.UnknownSymbol;
        }

        public static OperationType GetOperationType(string operatorStr)
        {
            foreach (var pair in OPERATOR_CHARACTER_TABLE)
            {
                if ( operatorStr == new string(pair.Value, 1) )
                {
                    return pair.Key;
                }
            }

            return OperationType.Unknown;
        }

        public static int GetOperatorPryority(OperationType operation)
        {
            const int PRIMARY_PRIORITY = 2;
            const int SECOND_PRIORITY = 1;

            if (operation == OperationType.Mul ||
                operation == OperationType.Div)
            {
                return PRIMARY_PRIORITY;
            }
            else 
            {
                return SECOND_PRIORITY;
            }
        }


        private static bool SymbolIsPartOfNumber(char symbol)
        {
            return ResultFindInTargetList(symbol, NUMBER_CHARACTERS_LIST);
        }
        
        private static bool SymbolIsArithmeticOperator(char symbol)
        {
            
            MyList<char> charList = new MyList<char>();

            foreach(var pair in OPERATOR_CHARACTER_TABLE)
            {
                charList.Add( pair.Value );
            }

            return ResultFindInTargetTable(symbol, charList);
        }


        private static bool ResultFindInTargetList(char inputChar, char[] targetCharList)
        {
            foreach (var s in targetCharList)
            {
                if (inputChar == s)
                {
                    return true;
                }
            }

            return false;
        }

        private static bool ResultFindInTargetTable(char inputChar, IList<char> targetCharList)
        {
            foreach (var s in targetCharList)
            {
                if (inputChar == s)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
