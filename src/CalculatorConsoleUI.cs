﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator.src
{
    class CalculatorConsoleUI : ICalcUI
    {        
        private string _exprForCalculate = "";
        private IStrCalculator _calc;

        const string CLEAR_DISPLAY = "clear";

        public CalculatorConsoleUI(IStrCalculator calcLogic)
        {
            _calc = calcLogic;
        }


        public void FirstExpressionInput()
        {
            InputAndTryCalcExpression("Input expression");
        }     


        private void InputAndTryCalcExpression(string header)
        {
            Console.WriteLine(header);
            _exprForCalculate = Console.ReadLine();
                       
            if (_exprForCalculate == CLEAR_DISPLAY)
            {
                Console.Clear();
                InputAndTryCalcExpression(header);
            }

            bool resultOk = false;
            string calcResult = "no result";
            string errMsg = "";


            resultOk = _calc.TryCalcExpression(_exprForCalculate, ref calcResult, ref errMsg);


            if (resultOk)
            {
                Console.WriteLine("Calc result = {0}", calcResult);
                Console.WriteLine();

                InputAndTryCalcExpression("Input new expression");
            }
            else
            {
                Console.WriteLine(errMsg);
                Console.WriteLine();

                InputAndTryCalcExpression("Input new expression");
            }
        }

    }
}