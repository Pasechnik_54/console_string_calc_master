﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator.src
{
    interface IStrCalculator
    {
        bool TryCalcExpression(string expressionForCalc, ref string calcResult, ref string errorMsg);
    }
}
