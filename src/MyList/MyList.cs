﻿using System;
using System.Collections;
//using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyListContainer
{
    class MyList<T> : IList<T>, ICollection<T>
    {
       
        public int Count { get; private set; }
        
        public bool IsReadOnly { get; }
        
        public T this[int index] 
        { 
            get
            {
                if ( !IndexRangeIsValid(index) )
                {
                    throw new ArgumentOutOfRangeException("Error: incorrect index for get element");
                }
                
                return _array[index];
                    
            }
            
            set
            {
                if ( !IndexRangeIsValid(index) )
                {
                    throw new ArgumentOutOfRangeException("Error: incorrect index for set element");
                }
                // если место уже занято и список только для чтения
                if (index < Count - 1 && IsReadOnly)
                {
                    throw new NotSupportedException("Error: not allowed write to readonly value");
                }
                
                _array[index] = value;
            }
        }

        private T[] _array = null;
        
        private const int START_CAPACITY = 20;



        public MyList(bool readOnlyState = false)
        {
            Count = 0;
            IsReadOnly = readOnlyState;

            _array = new T[START_CAPACITY];            
        }           



        public void Add(T newElement)
        {
            if (IsReadOnly)
            {
                throw new NotSupportedException("insert operation is not allowed");
            }

            if (Count < _array.Length )
            {
                AddItemToArray(newElement);
            }
            else if (Count == _array.Length)
            {
                IncreaseSizeArray();
                AddItemToArray(newElement);
            }
            else
            {
                throw new IndexOutOfRangeException("next insert index out of range");
            }
        }
        #region Add functions

        private void AddItemToArray(T element)
        {
            _array[Count++] = element;
        }

        private void IncreaseSizeArray()
        {
            try
            {
                T[] copyArray = new T[_array.Length];
                Array.Copy(_array, copyArray, _array.Length);

                int newLength = _array.Length * 2; // увиличиваем вдвое, чтобы добавление было O(n)
                _array = new T[newLength];

                Array.Copy(copyArray, _array, copyArray.Length);
            }
            catch(OutOfMemoryException e)
            {
                Console.WriteLine("allocation error, details: {0}", e.Message);
            }
        }
        #endregion

       
        public void Insert(int index, T value)
        {
            if ( !IndexRangeIsValid(index) )
            {
                throw new ArgumentOutOfRangeException("Error: no valid insert index");
            }
            if (IsReadOnly)
            {
                throw new NotSupportedException("Error: not allowed insert to readonly element");
            }

            if (Count + 1 <= _array.Length)
            {
                // смещаем элементы
                for (int i = Count; i > index; i--)
                {
                    _array[i] = _array[i - 1];
                }

                Count++;
                _array[index] = value;
            }

        }

        
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException("Error: destination array is null");
            }
            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException("Error: start copy index is less than zero");
            }
            if (arrayIndex > Count-1)
            {
                throw new ArgumentOutOfRangeException("Error: start copy index is great than maximum");
            }

            int copyQuantity = Count - arrayIndex;
            if (copyQuantity > array.Length)
            {
                throw new ArgumentException("Error: size destination array is small");
            }

            Array.Copy(_array, arrayIndex, array, 0, copyQuantity);

        }

        public void Clear()
        {          
            _array = new T[START_CAPACITY];
            Count = 0;
        }

        
        public int IndexOf(T value)
        {
            for (int i = 0; i < Count; i++)
            {
                if ( _array[i].Equals(value) )
                {
                    return i;
                }
            }

            const int DEFAULT_VOID_INDEX = -1;
            return DEFAULT_VOID_INDEX;
        }

        
        public void RemoveAt(int index)
        {
            if (IsReadOnly)
            {
                throw new NotSupportedException("Collection is read only");
            }
            if ( !IndexContainsInCollection(index) )
            {
                throw new ArgumentOutOfRangeException("Error: index removed element is not correct");
            }
            
            // смещаем элементы
            for (int i = index; i < Count - 1; i++)
            {
                _array[i] = _array[i + 1];
            }

            Count--;
        }

        
        public bool Remove(T item)
        {
            if (IsReadOnly)
            {
                throw new NotSupportedException("Collection is read only");
            }  

            for (int i = 0; i < Count; i++)
            {
                if( (object)_array[i] == (object)item )
                {
                    RemoveAt(i);
                    return true;
                }
            }

            return false;
        }

        
        public bool Contains(T item)
        {
            for( int i = 0; i < Count; i++ )
            {
                if ( (object)_array[i] == (object)item)
                {
                    return true;
                }
            }

            return false;
        }


        private bool IndexRangeIsValid(int checkIndex)
        {
            if (checkIndex >= 0 && checkIndex < _array.Length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IndexContainsInCollection(int checkIndex)
        {

            if (checkIndex >= 0 && checkIndex < Count)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        // для перечисления
        public IEnumerator<T> GetEnumerator()
        {
            return new MyListEnumerator<T>(_array, Count);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

    }
}
