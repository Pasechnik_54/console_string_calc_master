﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyListContainer
{
    class MyListEnumerator<T> : IEnumerator<T>
    {
        const int START_ENUM_POS = -1;
        private int _currPos = START_ENUM_POS;

        private T[] _array = null;
        private int _count = 0;

        public MyListEnumerator(T[] array, int count)
        {
            _array = array;
            _count = count;
        }

        public T Current
        {
            get
            {
                if (_currPos > START_ENUM_POS && _currPos < _count)
                {
                    return _array[_currPos];
                }
                else
                {
                    throw new Exception("Error: incorrect current position for enumerable");
                }

            }
        }
        
        object IEnumerator.Current => Current;
        

        public bool MoveNext()
        {
            if (_currPos < (_count - 1) )
            {
                _currPos++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            _currPos = START_ENUM_POS;
        }

        public void Dispose()
        { 
            IDisposable d = _array[_currPos] as IDisposable;
            if (d != null)
            {
                d.Dispose();
            }
        }
    }
}
