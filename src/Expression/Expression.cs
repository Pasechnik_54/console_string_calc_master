﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyListContainer;

namespace ConsoleCalculator.src.Expression
{
    class Expression
    {
        private IList<int> _operatorsRangeFind = new MyList<int>();               
        private IList<BraceInterval> _braceExprPosList = new MyList<BraceInterval>();       
        private IList<Item> _expr = new MyList<Item>();

        private List<Operator> _operatorsList = new List<Operator>();

        public Expression(IList<Item> calcExpression)
        {            
            _expr = calcExpression;

            RemoveUnnecessaryParentheses();
        }

        public string GetCalculatedExpression(ref string errorMsg)
        {
            FillOperatorRangeFind();
            FillOperatorsList();
            
            bool resultOk = Calc(ref errorMsg);

            if (resultOk)
            {
                return _expr[0].StringExpression;
            }
            else
            {
                return "";
            }
            
        }

        private bool Calc(ref string errMsg)
        {

            do
            {
                foreach (var op in _operatorsList)
                {
                    // если у нас минус в начале выражения (т.е. отрицательное число)
                    if (op.Operation == OperationType.Diff &&
                        op.PositionIndex == 0)
                    {
                        Item number = GetRightOperand(op.PositionIndex);

                        if (number.Type == ItemType.Number)
                        {
                            ReplaceToNegativeNumb(op);
                        }
                        else
                        {
                            errMsg = "Error: incorrect operand after operator -";
                            return true;
                        }

                        break;                            
                    }
                    
                    Item left = GetLeftOperand(op.PositionIndex);
                    Item right = GetRightOperand(op.PositionIndex);

                    if ( left.Type == ItemType.Number &&
                         right.Type == ItemType.Number )
                    {
                        float leftOperand = (float)Convert.ToDouble(left.StringExpression);
                        float rightOperand = (float)Convert.ToDouble(right.StringExpression);

                        float res = CalcExpr(leftOperand, rightOperand, op, ref errMsg);
                        string resExpr = res.ToString();

                        ReplaceToCalcExpr(op, resExpr);

                        break;
                    }
                    // когда у '-' нет левого операнда (отрицательное число в скобках)
                    else if ( left.Type == ItemType.OpenBrace &&
                              right.Type == ItemType.Number &&
                              op.Operation == OperationType.Diff )
                    {
                        ReplaceToNegativeNumb(op);
                        break;
                    }
                    // если есть выражение в скобках, уходим в рекурсию для вычисления
                    else if ( left.Type == ItemType.CloseBrace)
                    {
                        RecursiveCalc(false, (op.PositionIndex - 1), ref errMsg);
                        break;
                    }
                    else if (right.Type == ItemType.OpenBrace)
                    {
                        RecursiveCalc(true, (op.PositionIndex + 1), ref errMsg);
                        break;
                    }
                    
                }

                FindAllBracesPositions();
                FillOperatorRangeFind();
                FillOperatorsList();

            } while (_operatorsList.Count > 0);

            return true;
        }

        private void ReplaceToNegativeNumb(Operator diffOp)
        {
            Item number = GetRightOperand(diffOp.PositionIndex);

            string res = "-" + number.StringExpression;
            _expr[diffOp.PositionIndex] = new Item(ItemType.Number, res);
            _expr.RemoveAt(diffOp.PositionIndex + 1);
            _operatorsList.Remove(diffOp);
        }

        private void ReplaceToCalcExpr(Operator op,  string insExpr)
        {
            _expr[op.PositionIndex - 1] = new Item(ItemType.Number, insExpr);

            _expr.RemoveAt(op.PositionIndex + 1);
            _expr.RemoveAt(op.PositionIndex);
            _operatorsList.Remove(op);
        }

        private void RemoveofRange(int startIndex, int endIndex)
        {
            for ( int i = endIndex; i > startIndex; i--)
            {
                _expr.RemoveAt(i);
            }
        }

        private void RecursiveCalc(bool openIndex, int targetIndex, ref string errMsg)
        {

            foreach (var pair in _braceExprPosList)
            {
                int findIndex = 0;

                if (openIndex)
                {
                    findIndex = pair.openIndex;
                }
                else
                {
                    findIndex = pair.closeIndex;
                }

                if (findIndex == targetIndex)
                {
                    MyList<Item> newExpr = new MyList<Item>();

                    for (int i = pair.openIndex + 1; i < pair.closeIndex; i++)
                    {
                        newExpr.Add(_expr[i]);
                    }

                    Expression exprPart = new Expression(newExpr);

                    string calcExpr = exprPart.GetCalculatedExpression(ref errMsg);

                    _expr[pair.openIndex] = new Item(ItemType.Number, calcExpr);

                    RemoveofRange(pair.openIndex, pair.closeIndex);

                    _braceExprPosList.Remove(pair);
                    break;
                }
            }
        }


        private void FillOperatorsList()
        {
            _operatorsList.Clear();

            foreach (var i in _operatorsRangeFind)
            {                
                if (_expr[i].Type == ItemType.BinaryOperator)
                {
                    OperationType opType = ValidSymbolsTable.GetOperationType(_expr[i].StringExpression);
                    Operator op = new Operator(opType, i);
                    _operatorsList.Add(op);
                }                
            }

            _operatorsList.Sort();

            // debug
            /*
            Console.WriteLine("operators list in sub-expression");
            foreach (var op in _operatorsList)
            {
                Console.WriteLine(" operation {0} in pos {1};", op.Operation, op.PositionIndex);
            }
            */
        }

        private void FillOperatorRangeFind()
        {
            _operatorsRangeFind.Clear();

            if (_braceExprPosList.Count != 0)
            {
                for (int i = 0; i < _expr.Count; i++)
                {
                    if ( IndexInsideBraces(i) )
                    {
                        continue;
                    }
                    else
                    {
                        _operatorsRangeFind.Add(i);
                    }
                }
            }
            else // если скобок нет, то будем искать во всем диапазоне
            {
                for (int i = 0; i < _expr.Count; i++)
                {
                    _operatorsRangeFind.Add(i);
                }
            }

            // debug
            /*
            Console.WriteLine("indexses of operators find:");
            foreach (var i in _operatorsRangeFind)
            {
                Console.Write(i + " ");                
            }
            Console.Write("\n");
            */
        }

        private bool IndexInsideBraces(int index)
        {
            foreach (var pairPos in _braceExprPosList)
            {
                if (index >= pairPos.openIndex && index <= pairPos.closeIndex)
                {
                    return true;
                }
            }

            return false;
        }


        private Item GetLeftOperand(int operatorIndex)
        {
            return _expr[operatorIndex - 1];
        }

        private Item GetRightOperand(int operatorIndex)
        {
            return _expr[operatorIndex + 1]; ;
        }

        private float CalcExpr(float left, float right, Operator op, ref string errMsg)
        {
            switch (op.Operation)
            {
                case OperationType.Mul:
                    {
                        return left * right;
                    }
                case OperationType.Div:
                    {
                        if (right != 0)
                        {
                            return left / right;
                        }
                        else
                        {
                            errMsg = "Error: division by zero attempt";
                            return 0;
                        }                  
                    }
                case OperationType.Sum:
                    {
                        return left + right;
                    }
                case OperationType.Diff:
                    {
                        return left - right;
                    }
                default:
                    {
                        return 0;
                    }
            }
        }

        private void FindAllBracesPositions()
        {
            _braceExprPosList.Clear();
            Stack<int> stackOpenBracePos = new Stack<int>();

            for (int i = 0; i < _expr.Count; i++)
            {

                Item currItem = _expr[i];
                ItemType type = currItem.Type;

                switch (type)
                {
                    case ItemType.OpenBrace:
                        {
                            stackOpenBracePos.Push(i);
                        }
                        break;
                    case ItemType.CloseBrace:
                        {
                            // когда встречаем закрывающую скобку, получаем индекс 
                            // предшествовавшей ей открывающей и запоминаем позицию выражения
                            int openPos = stackOpenBracePos.Pop();
                            BraceInterval braceExpression = new BraceInterval(openPos, i);
                            _braceExprPosList.Add(braceExpression);
                        }
                        break;
                }


            }

        }


        private void RemoveUnnecessaryParentheses()
        {
            FindAllBracesPositions();

            for (int i = 0; i < _braceExprPosList.Count; i++)
            {
                var pair = _braceExprPosList[i];

                // debug
                /*
                Console.WriteLine("pair braces position {0} {1}",
                                   pair.openIndex, pair.closeIndex);
                */


                // если пара скобок по краям всего выражения, отбрасываем их
                if (pair.openIndex == 0 && pair.closeIndex == _expr.Count - 1)
                {
                    TrimSides(i);

                    FindAllBracesPositions(); // пересчет индексов
                    RemoveUnnecessaryParentheses(); // перезапускаем цикл, пока все скобки по краям не будут удалены                    
                }
            }
        }


        private void TrimSides(int currIndex)
        {
            var pairPos = _braceExprPosList[currIndex];

            // debug
            /*
            Console.WriteLine("Expand braces, pos: {0} {1}",
                              pairPos.openIndex, pairPos.closeIndex);
            */

            _expr.RemoveAt(0);
            _expr.RemoveAt(_expr.Count - 1);

            _braceExprPosList.RemoveAt(currIndex);
        }
    }
}
