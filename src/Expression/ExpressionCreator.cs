﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator.src.Expression
{
    class ExpressionCreator
    {
        IList<Item> _itemList = null;
        List<BraceInterval> _braceExprPosList = new List<BraceInterval>();

        // корневой элемент дерева инструкций
        Expression _exprForCalc = null;

        public bool CreateCalcExpression(IList<Item> itemList, ref string errorMsg)
        {
            _itemList = itemList;
            _braceExprPosList.Clear();

            bool checResultOk = CheckExpressionAndFindBracesIntervals(ref errorMsg);
            if (!checResultOk)
            {
                return false;
            }

            _exprForCalc = new Expression(_itemList);

            return true;
        }

        public string CalcExpression(ref string errorMsg)
        {
            return _exprForCalc.GetCalculatedExpression(ref errorMsg);
        }


        private bool CheckExpressionAndFindBracesIntervals(ref string errorMsg)
        {
            string errHeader = "Error: ";

            Stack<int> stackOpenBracePos = new Stack<int>();

            for (int i = 0; i < _itemList.Count; i++)
            {
                Item currItem = _itemList[i];

                ItemType type = currItem.Type;

                switch (type)
                {
                    case ItemType.OpenBrace:
                        {
                            stackOpenBracePos.Push(i);
                        }
                        break;
                    case ItemType.CloseBrace:
                        {
                            // если эта скобка не последняя, проверяем, что справа от нее стоит оператор
                            if ( !EndExpression(i) )
                            {
                                ItemType nextItemType = GetTypeNextElement(i);

                                if (nextItemType != ItemType.BinaryOperator &&
                                    nextItemType != ItemType.CloseBrace)
                                {
                                    errorMsg = errHeader + "incorrect symbol after closed parenthes";
                                    return false;
                                }
                            }

                            // когда встречаем закрывающую скобку, получаем индекс 
                            // предшествовавшей ей открывающей и запоминаем позицию выражения
                            int openPos = stackOpenBracePos.Pop();
                            BraceInterval braceExpression = new BraceInterval(openPos, i);
                            _braceExprPosList.Add(braceExpression);
                        }
                        break;
                    case ItemType.BinaryOperator:
                        {
                            // проверяем, что слева и справа от оператора стоит либо число,
                            // либо соответствующая скобка

                            OperationType operType = ValidSymbolsTable.GetOperationType(currItem.StringExpression);
                           
                            if( StartExpression(i) )
                            {
                                if (operType != OperationType.Diff)
                                {
                                    errorMsg = string.Format("{0}operator {1} cannot be at the beginning of expression",
                                                         errHeader, currItem.StringExpression);
                                    return false;
                                }                                
                            }
                            else if ( EndExpression(i) )
                            {
                                errorMsg = errHeader + "operator cannot be at the end of expression";
                                return false;
                            }
                            else
                            {
                                ItemType nextItemType = GetTypeNextElement(i);
                                if (nextItemType != ItemType.Number &&
                                    nextItemType != ItemType.OpenBrace)
                                {
                                    errorMsg = string.Format("{0}incorrect operand to right the operator: {1}", 
                                                             errHeader, currItem.StringExpression);

                                    return false;
                                }

                                
                                ItemType backItemType = GetTypeBackElement(i);
                                if (operType != OperationType.Diff)
                                {
                                    if (backItemType != ItemType.Number &&
                                        backItemType != ItemType.CloseBrace)
                                    {
                                        errorMsg = string.Format("{0}incorrect operand to left the operator: {1}",
                                                                 errHeader, currItem.StringExpression);

                                        return false;
                                    }
                                }
                                else
                                {
                                    if (backItemType != ItemType.Number &&
                                        backItemType != ItemType.CloseBrace &&
                                        backItemType != ItemType.OpenBrace )
                                    {
                                        errorMsg = string.Format("{0}incorrect operand to left the operator: {1}",
                                                                 errHeader, currItem.StringExpression);

                                        return false;
                                    }
                                }
                                
                            }
                        }
                        break;
                    case ItemType.Number:
                        {
                            // проверяем, что если слева и справа от числа стоят 
                            //либо операторы либо скобки (с учетом границ массива пр крайних положениях)
                            if ( !EndExpression(i) )
                            {
                                ItemType nextItemType = GetTypeNextElement(i);

                                if (nextItemType == ItemType.Number)
                                {
                                    errorMsg = errHeader + "not found operator between two numbers";
                                    return false;
                                }
                            }
                            if ( !StartExpression(i) )
                            {
                                ItemType backItemType = GetTypeBackElement(i); // back

                                if (backItemType == ItemType.Number)
                                {
                                    errorMsg = errHeader + "not found operator between two numbers";
                                    return false;
                                }
                            }
                        }
                        break;
                }
            }

            if (stackOpenBracePos.Count != 0)
            {
                errorMsg = errHeader + "incorrect quantity parentheses";
                return false;
            }            

            return true;
        }


        ItemType GetTypeNextElement(int currIndex)
        {
            return _itemList[currIndex + 1].Type;
        }

        ItemType GetTypeBackElement(int currIndex)
        {
            return _itemList[currIndex - 1].Type;
        }

        bool StartExpression(int currIndex)
        {
            if (currIndex == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool EndExpression(int currIndex)
        {
            if (currIndex == _itemList.Count - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
