﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator.src.Expression
{
    enum ItemType
    {
        Number,
        BinaryOperator,
        OpenBrace,
        CloseBrace
    }
}
