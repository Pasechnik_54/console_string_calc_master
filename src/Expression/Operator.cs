﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator.src.Expression
{ 
    class Operator : IComparable<Operator>
    {
        public OperationType Operation { get; }
        public int PositionIndex { get; }

        private int _priority = 0;

        public Operator(OperationType operatorType, int operatorPosIndex)
        {
            PositionIndex = operatorPosIndex;
            Operation = operatorType;
            _priority = ValidSymbolsTable.GetOperatorPryority(operatorType);
        }

        public int CompareTo(Operator x)
        {
            return x._priority - this._priority;
        }

    }
}
