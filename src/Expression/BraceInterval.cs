﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator.src.Expression
{
    struct BraceInterval
    {
        public int openIndex;
        public int closeIndex;

        public BraceInterval(int openPosition, int closePosition)
        {
            openIndex = openPosition;
            closeIndex = closePosition;
        }
    }
}
