﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalculator.src.Expression
{
    struct Item
    {
        public ItemType Type { get; }
        public string StringExpression { get; }

        public Item(ItemType type, string exprValue)
        {
            Type = type;
            StringExpression = exprValue;
        }

    }
}
